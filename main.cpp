#include <math.h>
#include <string>
#include <set>
#include <iostream>
#include <map>
#include <vector>
#include <algorithm>
#include <filesystem>

#include "args.hxx"
#include "hdf5.h"
#include "silo.h"


std::string extract_var_name(std::string var_name_w_meshid){
    std::string extracted_var  = "";
    // Find position of last underscore '_'
    std::size_t pos_us = var_name_w_meshid.find_last_of("_");
    if (pos_us != std::string::npos){
        std::string end_of_var_name = var_name_w_meshid.substr(pos_us, var_name_w_meshid.length() - pos_us);
        if (isdigit(end_of_var_name[end_of_var_name.length() - 1])){
            
            extracted_var = var_name_w_meshid.substr(0, pos_us);
        }
    }
    
    return extracted_var;
}


int main(int argc, char* argv[]){
    std::vector<std::string> silo_filenames;
    std::string h5_outputdir = "";

    args::ArgumentParser parser("Converts .silo files to .h5", "");
    args::HelpFlag help(parser, "help", "Display this help menu", {'h', "help"});
    
    args::Group group(parser, "Pass a single Silo file or a directory containing Silo files:", args::Group::Validators::Xor);
    
    args::ValueFlag<std::string> silo_filename_arg(group, "silo_filename", "Silo file to convert", {'f', "file"});
    args::ValueFlag<std::string> silo_dir_arg(group, "silo_dir", "Directory containing Silo files to convert", {'d', "dir"});

    args::ValueFlag<std::string> h5_outputdir_arg(parser, "h5_outputdir", "Directory to place converted .h5 files in (default: same directory as .silo)", {'o', "od"});

    args::Flag verbose(parser, "verbose", "Include more converter output.", {'v', "verbose"});
    args::Flag skip(parser, "skip", "Skip already created .h5 files.", {'s', "skip"});

    try{
        parser.ParseCLI(argc, argv);
    }
    catch (const args::Completion& e){
        std::cout << e.what();
        return 0;
    }
    catch (const args::Help&){
        std::cout << parser;
        return 0;
    }
    catch (const args::ParseError& e){
        std::cerr << e.what() << std::endl;
        std::cerr << parser;
        return 1;
    }
    catch (const args::ValidationError& e){
        std::cerr << "Pass a single filename OR a directory. Not both." << std::endl;
        return 1;
    }
    if (silo_filename_arg){
        std::filesystem::path silo_filename = std::filesystem::canonical(args::get(silo_filename_arg));
        std::cout << "Converting " << silo_filename << std::endl;
        silo_filenames.push_back(args::get(silo_filename_arg));
    }
    if (silo_dir_arg){
        std::filesystem::path silo_dir = std::filesystem::canonical(args::get(silo_dir_arg));
        std::cout << "Converting all .silo files within " << silo_dir << std::endl; 
        for (std::filesystem::path file: std::filesystem::directory_iterator(args::get(silo_dir_arg))){

            if (file.extension() == ".silo"){
                silo_filenames.push_back(std::filesystem::canonical(file));
            }

        }
    }
    if (h5_outputdir_arg){
        h5_outputdir = std::filesystem::canonical(args::get(h5_outputdir_arg));
        std::cout << "Outputting all converted .h5 files to " << h5_outputdir << std::endl;
    }

    for (std::string silo_filename: silo_filenames){
        // Output filename
        std::filesystem::path hdf5_file(silo_filename);
        hdf5_file.replace_extension(".h5");

        if (h5_outputdir.size() > 0){
            hdf5_file = h5_outputdir / hdf5_file.filename();
        }

        if (skip){
            if (std::filesystem::exists(hdf5_file)){
                if (verbose){
                    std::cout << hdf5_file << " exists already. Skipping." << std::endl;
                }
                continue;
            }
        }

        int err = 0;
        DBfile* dbfile;
        
        std::set<std::string> unique_var_names;
        int nqmesh = 0;

        std::map<std::string, std::vector<double>> data;

        if (verbose){
            std::cout << "Converting " << silo_filename << " ";
        }


        dbfile = DBOpen(silo_filename.c_str(), DB_UNKNOWN, DB_READ);
        err = DBSetDir(dbfile, "data");

        DBtoc* dbtoc = DBGetToc(dbfile);
        if (dbtoc) {
            int nqvar = dbtoc->nqvar;
            nqmesh = dbtoc->nqmesh;
        
            // nqmesh counts bulk and fine mesh.
            // qvar data only on fine mesh so need to halve nqmesh
            nqmesh *= 0.5;
            for (int idx_var = 0; idx_var < nqvar ; idx_var+=1){
                std::string var_name = std::string(dbtoc->qvar_names[idx_var]);
                std::string extracted_var = extract_var_name(var_name);

                unique_var_names.insert(extracted_var);
            }

            for (const std::string& unique_var_name: unique_var_names){
                data[unique_var_name] = std::vector<double>();
            }
        }

        data["x"] = std::vector<double>();
        data["y"] = std::vector<double>();

        for (int idx_mesh = 1; idx_mesh <= nqmesh; idx_mesh++){
            std::string mesh_name = "gg" + std::to_string(idx_mesh);

            DBquadmesh* dbqm = DBGetQuadmesh(dbfile, mesh_name.c_str());
            void* coords = dbqm->coords;
            int datatype = dbqm->datatype;
            int* dims = dbqm->dims;

            if (datatype == DB_DOUBLE){
                double** conv_coords = (double**)coords;

                std::vector<double> x_coord_vec(conv_coords[0], conv_coords[0] + dims[0]);
                double delta_x = x_coord_vec[1] - x_coord_vec[0];
                std::vector<double> y_coord_vec(conv_coords[1], conv_coords[1] + dims[1]);
                double delta_y = y_coord_vec[1] - y_coord_vec[0];

                // coordinates are nodal, but variables are cell centered. We want our coordinates to also be
                // cell centered. n_cells = n_nodes - 1. x_cell[0] = x_node[0] + 0.5 * delta_x
                std::vector<double> x_coord_grid((x_coord_vec.size() - 1) * (y_coord_vec.size() - 1), 0);
                std::vector<double> y_coord_grid((x_coord_vec.size() - 1) * (y_coord_vec.size() - 1), 0);

                for (int idx_y = 0; idx_y < y_coord_vec.size() - 1; idx_y++){
                    for (int idx_x = 0; idx_x < x_coord_vec.size() - 1; idx_x++){
                        int idx_elem = idx_y * (x_coord_vec.size() - 1) + idx_x;
                        x_coord_grid[idx_elem] = x_coord_vec[idx_x] + 0.5 * delta_x;
                        y_coord_grid[idx_elem] = y_coord_vec[idx_y] + 0.5 * delta_y;
                    }
                }

                data["x"].insert(data["x"].end(), x_coord_grid.begin(), x_coord_grid.end());
                data["y"].insert(data["y"].end(), y_coord_grid.begin(), y_coord_grid.end());
            }
            DBFreeQuadmesh(dbqm);

            for (std::string unique_var_name: unique_var_names){
                std::string var_name = unique_var_name + "_" + std::to_string(idx_mesh);
                
                DBquadvar* dbqv1 = DBGetQuadvar(dbfile, var_name.c_str());
                int major_order = dbqv1->major_order;
                double* vals = (double*)dbqv1->vals[0];
                int n_elems = dbqv1->nels;
                std::vector<double> vals_vec(vals, vals + n_elems);
                data[unique_var_name].insert(data[unique_var_name].end(), vals_vec.begin(), vals_vec.end());
                DBFreeQuadvar(dbqv1);
            }
        }

        DBClose(dbfile);

        unique_var_names.insert("x");
        unique_var_names.insert("y");
        
        hid_t file_id;
        herr_t status;
        hid_t dataspace_id;
        hid_t dataset_id;
        hsize_t dims[] = {data["x"].size()};

        
        /* Create a new file using default properties. */
        file_id = H5Fcreate(hdf5_file.string().c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
        
        /* Create the data space for the dataset. */
        dataspace_id = H5Screate_simple(1, dims, NULL);

        for (std::string unique_var_name: unique_var_names){

            dataset_id = H5Dcreate2(file_id, std::string("/" + unique_var_name).c_str(), H5T_NATIVE_DOUBLE, dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

            status = H5Dwrite(dataset_id, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, data[unique_var_name].data());

            status = H5Dclose(dataset_id);
        }

        status = H5Sclose(dataspace_id);


        /* Terminate access to the file. */
        status = H5Fclose(file_id); 

        if (verbose){
            std::cout << "--> Done (" << hdf5_file << ")" << std::endl;
        }
    }
        

    return 0;
}