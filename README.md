# Single file conversion

Passing a single `.silo` filename as an argument to the program preceded by `-f` will convert that file e.g.:

```sh
./convert_silo -f my_file.silo
```

This will create `my_file.h5`

# Directory conversion

Passing the directory containing `.silo` files as an argument preceded by `-d` will convert all `.silo` files in that directory e.g.:

```sh
./convert_silo -d my_simulation_results
```

This will create a `.h5` file within the passed directory for each `.silo` file in that directory.

# Additional CLI arguments

## Changing output directory

In both single file mode and directory mode you can change the output directory of the converted `.h5` files with the `--od` flag e.g.:

```sh
./convert_silo -f my_file.silo --od=/my/h5/dir
```

This will create `/my/h5/dir/my_file.h5`

## Skip files already created

By adding the flag `-s` or `--skip` the converter will check if the `.h5` file it was planning to create already exists. If it does it will skip the conversion.

## Increase verbosity

You can add the `-v` or `--verbose` flag to increase output of the converter. It will print out the full path of the `.silo` file and of the resulting `.h5` file. It will also print out which file it has skipped when using `-s`


# Compiling
Build files (e.g Makefiles) are created using CMake.

## Basic

```sh
cd build
cmake ..
make
```

## Configuration options
CMake needs to know the location of your Silo include and library directory. By default Silo installs to `/usr/local`, but this can be different on your machine. You can change these during the CMake command like:

```sh
cd build
cmake .. -DSILO_INCLUDE_DIRS=~/silo-4.10.2-bsd/include -DSILO_LIB_DIRS=~/silo-4.10.2-bsd/lib
make
```

The HDF5 library is automagically looked for using CMakes `find_package()`. This should work if HDF5 was properly installed on the machine.

# Requirements

The conversion tool requires that silo and hdf5 libraries are installed.

silo: https://wci.llnl.gov/simulation/computer-codes/silo

hdf5: https://www.hdfgroup.org/downloads/hdf5/source-code/
