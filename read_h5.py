import h5py
import matplotlib.pyplot as plt

f = h5py.File("test.h5", 'r')

print(list(f.keys()))


electric_fld = f["electric_fld"]
y = f["y"]


plt.figure()
plt.plot(y[:], electric_fld[:])
plt.show()

f.close()
